### Game objects ###
--------------------
+ **barrier:** This is the object that protects the shooter from the aliens' bullets.
    + A part of the barrier is destroyed each time it is hit by a bullet whether it be by an alien or the user.
+ **alienShooter:** This is the users gun used to shoot down the aliens. Each time the alienShooter is hit, the user looses a life.
+ **purpleAlien:** A purple octopus looking alien, worth 30pts.
    + If hit it's by a user's bullet, then it is removed from the screen.
+ **redAlien:** A red alien, worth 20 pts.
    + If hit it's by a user's bullet, then it is removed from the screen.
+ **blueAlien:** A blue spider looking alien, worth 10pts.
    + If hit it's by a user's bullet, then it is removed from the screen.

### Animation of Objects ###
----------------------------
+ All of the animation of the aliens come from a single sprite sheet, I used two images for each alien animation to make it look as if they're walking. I used setinterval with an interval of 90 secs (or 1500 millisecs) to make a random alien shoot. I used an image for the alien shooter, this is animated based on the user's keystroke (move left, right, and shoot).

### Credits ###
---------------
+ Colene McFarlane
