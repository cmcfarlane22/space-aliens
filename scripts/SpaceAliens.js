var canvas
  , context
  , stage
  , width
  , height;
var isPlaying = new Boolean(false); //isPlaying set to false
var scoreTxt = new createjs.Text("", "10px 'press_start_2pregular'", "#FFF");
var score = 0;
var lives = new createjs.Text("", "10px 'press_start_2pregular'", "#FFF");
var livesLeft = 3;
var allAliens; //array for all our aliens
var totalAlienCount; //number of aliens on the canvas
var pos;
var whichShooter = 0;

var purpleAlienAnimation, purpleAliensArray;
var redAlienAnimation, redAliensArray;
var blueAlienAnimation, blueAliensArray;
var shooterAnimation, shooterAnimList;
//var barrierAnimation, barrierAnimList;

var barrier1 = [] //holds all the sqaures that make up the three barriers
  , barrier2 = []
  , xPos = []
  , yPos = []
  , squares = []
  , barrier3 = [];

var purpleAlien = new Image()
  , redAlien = new Image()
  , blueAlien = new Image()
  , alienShooter = new Image()
  , barrier = new Image();

var alienBullets
  , alienBulletSpeed = 15;
var roundBullets
  , roundBulletGraphics;
var barrierShape
  , barrierGraphics;

var shooterBullets; //our bullets
var bulletG;
var shooterBulletSpeed = 15; //speed of our bullets

var alienBullets;
var curScaleX = 1
  , curScaleY = 1;
var barriers = 3;

//frame rate
var fps = 5;


/******************* HELPER FUNCTIONS *******************/
function handleImageLoad(e) {
	console.log(e.target.src + " loaded");
}

//called if there is an error loading the image (usually due to a 404)
function handleImageError(e) {
	console.log("Error Loading Image : " + e.target.src);
}

function handleKeyUp(e) {
	//handling keyboard presses
	if (isPlaying) {
		if (!e) { var e = window.event; }
		//if(e.keyCode == 32) { e.keyCode = 0; return false; }
		switch (e.keyCode) {
			case 32: e.keyCode = 0; return false;
			case 38:
				var s = getSprite(shooterAnimation);
				shooterBullets.push(s);
				stage.addChild(s);
				//alert("in keyUp");
				break;
			case 37:
				//shooterAnimation.x = shooterAnimation.x; //can't move to the left any more
				break;
			case 39:
				//shooterAnimation.x = shooterAnimation.x; //can't move to the right any more
				break;
		}
	}
}

function handleKeyDown(e) {
	//handling keyboard presses
	if (isPlaying) {
		if (!e) { var e = window.event; }
		switch (e.keyCode) {
			case 37:
				if (shooterAnimation.x <= width-(width-109)) {
					shooterAnimation.x = shooterAnimation.x; //can't move to the left any more
				}
				else { shooterAnimation.x -= shooterAnimation.vX; }
				break;
			case 39:
				if (shooterAnimation.x >= width - 220) {
					console.log(shooterAnimation.x);
					//shooterAnimation.x = shooterAnimation.x; //can't move to the right any more
					/*alert("X = " + shooterAnimation.x);*/
				}
				else { shooterAnimation.x += shooterAnimation.vX; }
				break;
		}
	}
}

function checkPos() {
	//if an alien reaches the bottom, it's game over
	for (var g = 0; g < allAliens.length; g++) {
		if (allAliens[g].y >= (height-220)) {
			endScreen();
		}
	}
}

function getSprite(shooter) {
	var bitmap = new createjs.Bitmap("img/bullet.png");
	bitmap.x = (shooter.x + shooter.cx)+2.5;
	bitmap.y = shooter.y;
	return bitmap;
}

/******************* MAIN GAME FUNCTIONS *******************/
/**
function controls() {
	alert("Move the shooter with the left and right arrow keys, use the up arrow key to shoot. Close this window and press the start button begin.");
}*/

function winScreen() {
	isPlaying = false;
	stage.removeAllChildren();
	createjs.Ticker.removeAllListeners();
	stage.update();
	document.getElementById("winMsg").style.fontSize = "98px";
	document.getElementById("winMsg").style.visibility = "visible";
}

function endScreen() {
	isPlaying = false;
	stage.removeAllChildren();
	createjs.Ticker.removeAllListeners();
	stage.update();
	document.getElementById("loseMsg").style.fontSize = "108px";
	document.getElementById("loseMsg").style.visibility = "visible";
	resetGame();
}
/*
function startScreen() {
	isPlaying = false;
	startMsg.x = 225;
	startMsg.y = height / 2 - 70;
	stage.addChild(startMsg);
	stage.update();
}
*/
function animatePurpleAliens() {
	var len = purpleAliensArray.length;

	for (var j = 0; j < len; j++) {
		purpleAnim = purpleAliensArray[j];
		//if we hit our imaginary wall, go in the opposite direction
		if (purpleAliensArray[len - 1].x >= width - 135) {
			purpleAnim.direction = "left";
			purpleAnim.y += 35;
		}
		else if (purpleAliensArray[len - 1].x <= (width / 2) + 25) {
			if (purpleAnim.direction == "left" && purpleAliensArray[0].x < 90) {
				purpleAnim.y += 35;
			}
			purpleAnim.direction = "right";
		}
		//move aliens from left to right
		if (purpleAnim.direction == "right") {
			purpleAnim.x += purpleAnim.vX;
		}
		else if (purpleAnim.direction == "left") {
			purpleAnim.x -= purpleAnim.vX;
		}
	}
}

function animateRedAliens() {
	var len = redAliensArray.length;

	for (var j = 0; j < len; j++) {
		redAnim = redAliensArray[j];
		if (redAliensArray[len - 1].x >= width - 135) {
			redAnim.direction = "left";
			redAnim.y += 35;
		}
		if (redAliensArray[len - 1].x <= (width / 2) + 25) {
			if (redAnim.direction == "left" && redAliensArray[0].x < 90) {
				redAnim.y += 35;
			}
			redAnim.direction = "right";
		}
		//move aliens from left to right
		if (redAnim.direction == "right") {
			redAnim.x += redAnim.vX;
		}
		else if (redAnim.direction == "left") {
			redAnim.x -= redAnim.vX;
		}
	}
}

function animateBlueAliens() {
	var len = blueAliensArray.length;

	for (var j = 0; j < len; j++) {
		blueAnim = blueAliensArray[j];
		if (blueAliensArray[len - 1].x >= width - 135) {
			blueAnim.direction = "left";
			blueAnim.y += 35;
		}
		if (blueAliensArray[len - 1].x <= (width / 2) + 25) {
			if (blueAnim.direction == "left" && blueAliensArray[0].x < 90) {
				blueAnim.y += 35;
			}
			blueAnim.direction = "right";
		}
		//move aliens from left to right
		if (blueAnim.direction == "right") {
			blueAnim.x += blueAnim.vX;
		}
		else if (blueAnim.direction == "left") {
			blueAnim.x -= blueAnim.vX;
		}
	}
}

function createBarrier(){
	var barrier_xPos = [146, 135, 124, 124, 124, 124, 201, 212];
	var barrier_yPos = [420, 431, 442, 453, 464, 475, 464, 475];
	console.log("Hi");
	for (var i=0; i<52; i++){
		if(i <= 6){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[0];
			shooterBarrier.y = barrier_yPos[0];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			barrier_xPos[0] += 11;
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[0], barrier_yPos[0], 9, 9);
			barrier1.push(s);
			stage.addChild(s);
			console.log("X:" + g.x);*/
		}
		else if(i >=7 && i <= 15){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[1];
			shooterBarrier.y = barrier_yPos[1];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[1], barrier_yPos[1], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[1] += 11;
		}
		else if(i >= 16 && i <= 26){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[2];
			shooterBarrier.y = barrier_yPos[2];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[2], barrier_yPos[2], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[2] += 11;
		}
		else if(i >= 27 && i <= 37){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[3];
			shooterBarrier.y = barrier_yPos[3];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[3], barrier_yPos[3], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[3] += 11;
		}
		else if(i >= 38 && i <= 41){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[4];
			shooterBarrier.y = barrier_yPos[4];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[4], barrier_yPos[4], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[4] += 11;
		}
		else if(i >= 42 && i <= 44){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[5];
			shooterBarrier.y = barrier_yPos[5];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[5], barrier_yPos[5], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[5] += 11;
		}
		else if(i >= 45 && i <= 48){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[6];
			shooterBarrier.y = barrier_yPos[6];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[6], barrier_yPos[5], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[6] += 11;
		}
		else if(i >= 49 && i <= 51){
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = barrier_xPos[7];
			shooterBarrier.y = barrier_yPos[7];
			xPos.push(shooterBarrier.x);
			yPos.push(shooterBarrier.y);
			//stage.addChild(shooterBarrier);
			/*g.beginStroke("#ff7bac");
			g.drawRect(barrier_xPos[7], barrier_yPos[6], 9, 9);
			barrier1.push(s);
			stage.addChild(s);*/
			barrier_xPos[7] += 11;
		}
	}
	drawBarrier();
}

function drawBarrier(){
	var shooterBarrier;

	for(var i=0; i<3; i++){
		for(var j=0; j<xPos.length; j++){
			if(i > 0){
				xPos[j] += 315;
			}
			shooterBarrier = new createjs.Shape(barrierGraphics);
			shooterBarrier.x = xPos[j];
			shooterBarrier.y = yPos[j];
			squares.push(shooterBarrier);
			stage.addChild(shooterBarrier);
		}
	}
	console.warn("Length of Array: " + squares.length);
}

function shoot(aliens, pos) {
	var alienProjectile = new createjs.Shape(roundBulletGraphics);
	//alienProjectile.scaleX = 1.5;
	alienProjectile.x = aliens[pos].x + 21;
	alienProjectile.y = aliens[pos].y + 42;
	alienBullets.push(alienProjectile);

	stage.addChild(alienProjectile);
}

function updateAlienBullets() {
	for (var i = 0; i < alienBullets.length; i++) {
		//console.error("Y = " + alienBullets[i].y);
		alienBullets[i].y += alienBulletSpeed;
		if (alienBullets[i].y >= height - 10) {
			stage.removeChild(alienBullets[i]);
			alienBullets.splice(i, 1);
		}
	}
	whichShooter = 1;
	hitBarrier(alienBullets);
	hitShooter();
}

function updateShooterBullets() {
	for (var i = 0; i < shooterBullets.length; i++) {
		var s = shooterBullets[i];
		s.y -= shooterBulletSpeed;
	}
	whichShooter = 2;
    hitBarrier(shooterBullets);
	hitAlien();
}

function hitBarrier(ammo) {
	for (var i = 0; i < ammo.length; i++) {
		for (var j = 0; j < squares.length; j++) {
			if (ammo[i].x >= squares[j].x && ammo[i].x < squares[j].x + 11) {
				if (ammo[i].y >= squares[j].y-11 && ammo[i].y < squares[j].y + 11) {
					console.error("Square " + j + " has been hit!!");
					stage.removeChild(squares[j]);
					stage.removeChild(ammo[i]);
					squares.splice(j, 1);
					ammo.splice(i, 1);
				}
			}
		}
	}

}


function hitAlien() {
	for (var i = 0; i < shooterBullets.length; i++) {
		for (var j = 0; j < allAliens.length; j++) {
			if (shooterBullets[i].x >= allAliens[j].x && shooterBullets[i].x <= allAliens[j].x + 42) {
				if (shooterBullets[i].y >= allAliens[j].y && shooterBullets[i].y <= allAliens[j].y + 42) {
					console.log("==================== WE HAVE A BLOODY HIT!! ====================");
					stage.removeChild(allAliens[j]);
					stage.removeChild(shooterBullets[i]);
					shooterBullets.splice(i, 1);
					if ((allAliens[j].name).indexOf("purple") != -1) {
						console.log(allAliens[j].name);
						score += 30;
					}
					else if ((allAliens[j].name).indexOf("red") != -1) {
						console.log(allAliens[j].name);
						score += 20;
					}
					else if ((allAliens[j].name).indexOf("blue") != -1) {
						console.log(allAliens[j].name);
						score += 10;
					}
					allAliens.splice(j, 1);

					if (allAliens.length == 0 && livesLeft >= 0) {
						winScreen();
					}
					console.log(score);
				}
			}
		}
	}
}

function hitShooter() {
	for (var i = 0; i < alienBullets.length; i++) {
		if (alienBullets[i].x >= shooterAnimation.x && alienBullets[i].x <= shooterAnimation.x + 85) {
			if (alienBullets[i].y >= shooterAnimation.y && alienBullets[i].y <= shooterAnimation.y + 45) {
				console.log("Shooter has been hit!!");
				stage.removeChild(alienBullets[i]);
				alienBullets.splice(i, 1);
				if (livesLeft == 0) {
					endScreen();
				}
				else {
					livesLeft -= 1;
				}
			}
		}
	}
}

/******************* MAIN GAME *******************/

function startGame() {
	//remove the overlay div, as we no longer need it
	//document.getElementById("titleView").style.visibility='hidden';
	var elem = document.getElementById("titleView");
	elem.parentNode.removeChild(elem);
	// create a new stage and point it at our canvas:
	isPlaying = true; //playing set to true
	stage = new createjs.Stage(canvas);
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;

	scoreTxt.x = 740;
	scoreTxt.y = 10;
	stage.addChild(scoreTxt);

	lives.x = 850;
	lives.y = 10;
	stage.addChild(lives);

	//make spritesheet
	var purpleAlienSS = new createjs.SpriteSheet({
		"animations": {
			"downPose": { "frames": [2, 5] }
		},
		"images": [purpleAlien],
		"frames": {
			"height": 50,
			"width": 50,
			"cx": 25,
			"cy": 25,
		}
	});

	//make spritesheet
	var redAlienSS = new createjs.SpriteSheet({
		"animations": {
			"downPose": { "frames": [0, 3] },
		},
		"images": [redAlien],
		"frames": {
			"height": 50,
			"width": 50,
			"cx": 17.5,
			"cy": 17.5,
		}
	});

	//make spritesheet
	var blueAlienSS = new createjs.SpriteSheet({
		"animations": {
			"downPose": { "frames": [1, 4] },
		},
		"images": [blueAlien],
		"frames": {
			"height": 50,
			"width": 50,
			"cx": 17.5,
			"cy": 17.5,
		}
	});

	//barrier spritesheet
	var barrierSS = new createjs.SpriteSheet({
		"animations": {
			"stationary": { "frames": [0] },
		},
		"images": [barrier],
		"frames": {
			"height": 52,
			"width": 90,
			"cx": 45,
			"cy": 18.5,
		}
	});

	//make spritesheet
	var shooterSS = new createjs.SpriteSheet({
		"animations": {
			"stationary": { "frames": [0] },
		},
		"images": [alienShooter],
		"frames": {
			"height": 52,
			"width": 92,
			"cx": 45,
			"cy": 18.5,
		}
	});

	createBarrier();

	// create a BitmapAnimation instance to display and play back the sprite sheet:
	purpleAlienAnimation = new createjs.BitmapAnimation(purpleAlienSS);
	redAlienAnimation = new createjs.BitmapAnimation(redAlienSS);
	blueAlienAnimation = new createjs.BitmapAnimation(blueAlienSS);
	//barrierAnimation = new createjs.BitmapAnimation(barrierSS);
	shooterAnimation = new createjs.BitmapAnimation(shooterSS);

	// start playing the first sequence:
	purpleAlienAnimation.gotoAndPlay("downPose"); 	//animate
	redAlienAnimation.gotoAndPlay("downPose"); 	//animate
	blueAlienAnimation.gotoAndPlay("downPose");
	//barrierAnimation.gotoAndPlay("stationary");
	shooterAnimation.gotoAndPlay("stationary");

	/******* Set up alien names and coordinates *******/
	purpleAlienAnimation.name = "purpleAlien";
	purpleAlienAnimation.direction = "right";
	purpleAlienAnimation.vX = 4;
	purpleAlienAnimation.x = 10;
	purpleAlienAnimation.y = 45;
	purpleAlienAnimation.cx = 21;

	redAlienAnimation.name = "redAlien";
	redAlienAnimation.direction = "right";
	redAlienAnimation.vX = 4;
	redAlienAnimation.x = 10;
	redAlienAnimation.y = 82.5;

	blueAlienAnimation.name = "blueAlien";
	blueAlienAnimation.direction = "right";
	blueAlienAnimation.vX = 4;
	blueAlienAnimation.x = 10;
	blueAlienAnimation.y = 159;

	shooterAnimation.name = "alienShooter";
	shooterAnimation.direction = "right";
	shooterAnimation.vX = 5;
	shooterAnimation.x = width / 2 - 56;
	shooterAnimation.y = 525;
	shooterAnimation.cx = 45; //will be useful when positioning bullets

	//number of aliens we want based on color
	var purpleAliens = 10;
	var redAliens = 20;
	var blueAliens = 20;


	//arrays to hold our aliens once created
	purpleAliensArray = [];
	redAliensArray = [];
	blueAliensArray = [];

	//add our aliens and shooter to the stage/canvas
	stage.addChild(purpleAlienAnimation);
	stage.addChild(redAlienAnimation);
	stage.addChild(blueAlienAnimation);
	stage.addChild(shooterAnimation);


	//lets make copies of our aliens
	/****Purple Aliens****/
	for (var i = 0; i < purpleAliens; i++) {
		purpleAlienAnimation.name = "purpleAlien" + i;
		purpleAlienAnimation.direction = "right";
		purpleAlienAnimation.vX = 4;
		purpleAlienAnimation.x += 50;
		purpleAlienAnimation.y = purpleAlienAnimation.y;

		purpleAlienAnimation.currentAnimationFrame = purpleAlienAnimation.currentAnimationFrame;

		stage.addChild(purpleAlienAnimation);
		purpleAliensArray.push(purpleAlienAnimation);

		if (i < purpleAliens - 1) { purpleAlienAnimation = purpleAlienAnimation.clone(); }
		createjs.Ticker.addListener(window);
	}

	/****Red Aliens****/
	for (var i = 0; i < redAliens; i++) {
		redAlienAnimation.name = "redAlien" + i;
		redAlienAnimation.direction = "right";
		redAlienAnimation.vX = 4;
		if (redAlienAnimation.x >= 510) {
			redAlienAnimation.x = 10;
			redAlienAnimation.y += 38.5;
		}
		redAlienAnimation.x += 50;
		redAlienAnimation.y = redAlienAnimation.y;

		redAlienAnimation.currentAnimationFrame = redAlienAnimation.currentAnimationFrame;

		stage.addChild(redAlienAnimation);
		redAliensArray.push(redAlienAnimation);

		if (i < redAliens - 1) { redAlienAnimation = redAlienAnimation.clone(); }
		createjs.Ticker.addListener(window);
	}

	/****Blue Aliens****/
	for (var i = 0; i < blueAliens; i++) {
		blueAlienAnimation.name = "blueAlien" + i;
		blueAlienAnimation.direction = "right";
		blueAlienAnimation.vX = 4;
		if (blueAlienAnimation.x >= 510) {
			blueAlienAnimation.x = 10;
			blueAlienAnimation.y += 37.5;
		}
		blueAlienAnimation.x += 50;
		blueAlienAnimation.y = blueAlienAnimation.y;

		blueAlienAnimation.currentAnimationFrame = blueAlienAnimation.currentAnimationFrame;

		stage.addChild(blueAlienAnimation);
		blueAliensArray.push(blueAlienAnimation);

		if (i < blueAliens - 1) { blueAlienAnimation = blueAlienAnimation.clone(); }
		createjs.Ticker.addListener(window);
	}

	allAliens = purpleAliensArray.concat(redAliensArray, blueAliensArray);
	totalAlienCount = allAliens.length;

	//make the aliens shoot
	setInterval(function () { shoot(allAliens, pos) }, 750);

	// we want to do some work before we update the canvas,
	// otherwise we could use Ticker.addListener(stage);
	createjs.Ticker.addListener(window);
	createjs.Ticker.useRAF = true;
	createjs.Ticker.setFPS(fps);

}

function resetGame(){
	score = 0;
	livesLeft = 3;
}

function init() {
	canvas = document.getElementById("testCanvas");
	context = canvas.getContext("2d");
	// grab canvas width and height for later calculations:
	width = canvas.width;
	height = canvas.height;

	/*context.font ="60px Merienda";
	context.fillStyle = "#FF0000";
	context.fillText("Click Start to play", 225, (height/2) - 70);
	context.fillText("Click Controls to view controls", ((width/2)-450), (height/2));*/

	//initialize score and lives left
	scoreTxt.text = "Score: " + score;
	lives.text = "Lives Left: " + livesLeft;
	shooterBullets = [];
	alienBullets = [];
	allAliens = [];

	//create the shape of alien bullets
	roundBullets = new createjs.Shape();
	roundBulletGraphics = new createjs.Graphics();
	roundBulletGraphics.setStrokeStyle(1);
	roundBulletGraphics.beginStroke("#F00"); //red outline
	roundBulletGraphics.beginFill("#FF0"); //yellow fill
	roundBulletGraphics.drawCircle(0, 0, 3);

	//create shooter barriers
	barrierShape = new createjs.Shape();
	barrierGraphics = new createjs.Graphics();
	barrierGraphics.beginFill("#0DA9D3");
	barrierGraphics.drawRect(0, 0, 11, 11);

	//handle image loading for all aliens
	purpleAlien.onload = handleImageLoad;
	purpleAlien.onerror = handleImageError;
	purpleAlien.src = "img/alienInvaders.png";

	redAlien.onload = handleImageLoad;
	redAlien.onerror = handleImageError;
	redAlien.src = "img/alienInvaders.png";

	blueAlien.onload = handleImageLoad;
	blueAlien.onerror = handleImageError;
	blueAlien.src = "img/alienInvaders.png";

	//handle image loading for alienShooter and barriers
	alienShooter.onload = handleImageLoad;
	alienShooter.onerror = handleImageError;
	alienShooter.src = "img/shooter.png";

	barrier.onload = handleImageLoad;
	barrier.onerror = handleImageError;
	barrier.src = "img/barrier.png";
}

function tick() {
	pos = Math.floor(Math.random() * purpleAliensArray.length);

	// Hit testing the screen width, otherwise our sprite would disappear

	/**Animating Purple Aliens*/
	animatePurpleAliens();
	animateRedAliens();
	animateBlueAliens();

	//check the y location of our aliens
	checkPos();

	/**Animating the Bullets*/
	updateAlienBullets();
	updateShooterBullets();

	scoreTxt.text = "Score: " + score;
	lives.text = "Lives Left: " + livesLeft;

	// update the stage:
	stage.update();
}
